---
layout: default
title: Articles
permalink: /articles
---

## Articles

<div class="mt-5">
  {% for post in site.posts %}

  <div class="card mb-4">
    <div class="card-body">
      <h5 class="card-title">{{ post.date | date: "%Y-%m-%d" }} - {{ post.title }}</h5>
      <p class="card-text">{{ post.description }}</p>
      <a href="{{ post.url }}" class="btn btn-primary stretched-link">Open</a>
    </div>
  </div>

  {% endfor %}
</div>
