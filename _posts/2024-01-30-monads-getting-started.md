---
title: "Monads: Getting Started"
description: "The odds that you have already used monads without being aware of it are very high. You find them quite often in programming, but they are not explicitly named as such."
keywords: ["Monads", "C#", "TypeScript"]
tags: [C#, TypeScript, Monads]
comments: true
---

The odds that you have already used monads without being aware of it are very high.
You find them quite often in programming, but they are not explicitly named as such.

If you're a C# developer who is familiar with `IEnumerable<T>` and its `SelectMany` extension method, you're halfway through understanding what monads are. The same applies to TypeScript developers who know about the `Promise` type and its `then` function.

By the end of this article, you will be able to recognize monads when you see them.
But be aware that you may not learn anything new here, only things you already know.

## Function Signatures

Let's start by making something simpler.
Function signatures can easily become complicated and tedious to work with.
Therefore, I'll use a different way to write signatures that makes them easier to read when they get out of hand.

Take this simple square function in C#:

```csharp
static int Square (int a) => a * a;
```

It has the following signature:

```csharp
int Square (int a)
```

The same can be written like this:

```fsharp
Square: int -> int
```

and read like: "`Square` is a function that takes an `int` as an argument and returns another `int`."  
Pretty simple, right?

Let's try a more comprehensive signature like Linq's Select extension method:

```fsharp
IEnumerable<TResult> Select<TSource,TResult> (this IEnumerable<TSource> source, Func<TSource,TResult> selector);
```

After rewriting, we get:

```fsharp
Select: IEnumerable<TSource> -> (selector: TSource -> TResult) -> IEnumerable<TResult>
```

and we can read it like:
"`Select` is a function that takes an `IEnumerable<TSource>` as the first argument, a selector/function of type `TSource -> TResult` as the second and returns an `IEnumerable<TResult>`."

Since we have removed the syntactical noise, it becomes much easier to read.
As stated above, the benefits show off when signatures become more complicated.

## Container Types

What do the following types have in common?

```fsharp
IEnumerable<T>   // C#
Promise<T>       // TypeScript
```

Both types are container types that can hold one or more values.
While `Promise<T>` can hold only one value, `IEnumerable<T>` is able to hold many values.
Of course, a promise may contain many values in the form of `Promise<T[]>`, but here `T[]` is seen as a single value.  
Now, let's define a generalization for such types.

```fsharp
M<a>
```

`M` is a container that can hold values of type `a`.  
`IEnumerable<T>` and `Promise<T>` are simply a `M<a>` in a more concrete perspective.

Since these types can hold values, it is also possible that they don't.
In this case, `IEnumerable<T>` is just an `Enumerable.Empty<T>`, and
a `Promise<T>` without a value is created by the `Promise<T>.reject` function.  

This is the crucial part: there must be a distinction between the two cases.  
**Either one or more values are present or not.**

## The Return Function

Next, we need a function to create such a container. That function is often called `return` and its type is `a -> M<a>`, which means, it takes a value of type `a` and returns a container of type `M<a>`.

In JavaScript, the `Promise<T>` type already has that function, it is named `resolve` and the signature is:

```typescript
PromiseConstructor.resolve<number>(value: number): Promise<number>

// Here, too, we can simplify it to
number -> Promise<number> // that is 'a -> M<a>' in a more concrete fashion.
```

In .NET such a `return` function doesn't exist as far as I know, so we have to create our own.

```fsharp
// since C# 12 we can use collection expressions
IEnumerable<T> Return<T> (T value) => [value];

// simplified signature
T -> IEnumerable<T>
```

Sure, IEnumerable is ubiquitous in .NET and you can create it in many different ways,
but for the sake of simplicity, I use it with only one value.

## The Bind Function

The bind function contains all the magic that makes monads so interesting.  
To put it in a nutshell, bind does this:

![Bind Function]({{ '/assets/img/Monad-Bind-Function.png' | relative_url }})

and has the signature:

```fsharp
bind: M<a> -> (binder: a -> M<b>) -> M<b>
```

First, it checks whether a value is present in the container; if it is, the value of type `a` gets unwrapped and passed to the binder function. The binder returns a new container that is finally returned from the bind function. You can see it as a transformation from `M<a>` to `M<b>` using the binder. But in the case where no value is available, the function returns an empty container.

Since C# and TypeScript are OO-stylish languages, we can put data and behavior together. By doing so, the bind function becomes part of the container type:

```fsharp
M<a>.bind: (binder: a -> M<b>) -> M<b>
```

Fortunately, `IEnumerable<T>.SelectMany` and `Promise<T>.then` have an overloading that satisfies this function signature.  

Here are examples in C#:

```csharp
// container with a value
var square = Return(5)
    // SelectMany takes an 'a' and returns an 'IEnumerable<b>'
    // 'b' has here the same type as 'a'
    .SelectMany<int, int>(x => [x * x]);
// Result: [25]

// container without a value
var noSquare = Enumerable.Empty<int>()
    // binder is not being called
    .SelectMany<int, int>(x => [x * x]);
// Result: IEnumerable.Empty<string>
```

and the equivalent in TypeScript:

```typescript
// container with a value
const square = Promise.resolve(5)
    // then: a -> Promise<a>
    .then((x) => Promise.resolve(x * x));
// Result: 25

// container without a value
const noSquare = Promise.reject()
    // not being called
    .then((x) => Promise.resolve(x * x))
    // JavaScript has a function that we can use to deal with that case
    .catch(() => console.log('no value present'));
// Result: no value present
```

Before we finish this section, there is one issue we should be aware of.  
Imagine you have an empty container, and the types `a` and `b` do not match:

```csharp
// works pretty fine
// even there is a type mismatch between 'a' and 'b'; int and string
var stringified = Enumerable.Empty<int>()
    .SelectMany<int, string>(x => [x.ToString()]);
// Result: IEnumerable.Empty<string>
```

Here, it works because C# does the conversion of an empty enumerable for us, but in more advanced scenarios, we have to do this on our own. More on that in an upcoming article.  

## Summary

To form a monad, you need:

* a container type
* a return function
* a bind function

Now, you're able to spot them in code.

However, the examples shown here are quite theoretical.  
In the next article about this topic, we will look at some more real-world use cases.
