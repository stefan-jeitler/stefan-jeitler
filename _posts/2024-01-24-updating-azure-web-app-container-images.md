---
title: "Updating Azure Web App Container Images"
description: Sometimes you just want to update the docker image of your Web App using the Azure Command-Line Interface (CLI) in the easiest possible way. This can be achieved  with ... 
keywords: [Azure az cli, Azure Web App Docker]
tags: [Azure, az-cli]
comments: true
---

Sometimes you just want to update the docker image of your Web App  
using the Azure Command-Line Interface (CLI) in the simplest way possible.  

This can be achieved with the following command:

<script src="https://gist.github.com/stefan-jeitler/faef385e6c3ae5a98eb05a389511df59.js"></script>

The backticks at the end are the powershell syntax for writing multi-line commands.

After the command has finished it may take a few moments till the new container is up and has replaced the current one.  
But the great thing here is there is no downtime.  

However, if you run a Production System in Azure  
you should definitely look into Deployment Slots.  
